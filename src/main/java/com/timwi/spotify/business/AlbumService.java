package com.timwi.spotify.business;

import com.timwi.spotify.model.Album;

import java.util.List;
import java.util.Optional;

public interface AlbumService {

    public List<Album> getAlbums(Optional<String> title, Optional<String> artist);
}
