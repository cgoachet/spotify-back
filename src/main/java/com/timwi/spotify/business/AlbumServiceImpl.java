package com.timwi.spotify.business;

import com.timwi.spotify.commons.exceptions.MissingRequestParamException;
import com.timwi.spotify.dao.SpotifyDAO;
import com.timwi.spotify.model.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.MissingRequiredPropertiesException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AlbumServiceImpl implements AlbumService {

    @Autowired
    private SpotifyDAO spotifyDAO;

    @Override
    public List<Album> getAlbums(Optional<String> title, Optional<String> artist) {
        if (title.isPresent() || artist.isPresent()) {
            return spotifyDAO.getAlbums(title.orElse(""), artist.orElse(""));
        } else {
            throw new MissingRequestParamException("Either title or artist should be provided");
        }
    }
}
