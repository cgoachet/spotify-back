package com.timwi.spotify.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Album {

    private String title;
    private String urlImage;
    private int duration;
    private Instant releaseDate;

    public Album(String title, String urlImage, int duration, Instant releaseDate) {
        this.title = title;
        this.urlImage = urlImage;
        this.duration = duration;
        this.releaseDate = releaseDate;
    }
}
