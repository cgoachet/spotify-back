package com.timwi.spotify.commons.exceptions;

import org.springframework.core.env.MissingRequiredPropertiesException;

public class MissingRequestParamException extends MissingRequiredPropertiesException {

    private String message;

    public MissingRequestParamException(String message) {
        super();
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
