package com.timwi.spotify.dao;

import com.timwi.spotify.model.Album;

import java.util.List;

public interface SpotifyDAO {

    public List<Album> getAlbums(String title, String artist);
}
