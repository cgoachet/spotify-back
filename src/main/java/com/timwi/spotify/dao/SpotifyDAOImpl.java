package com.timwi.spotify.dao;

import com.timwi.spotify.model.Album;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

@Repository
public class SpotifyDAOImpl implements SpotifyDAO {


    @Override
    public List<Album> getAlbums(String title, String artist) {
        return Arrays.asList(new Album("album 1", "", 36580, Instant.now()), new Album("album 2", "", 36580, Instant.now()));
    }

}
