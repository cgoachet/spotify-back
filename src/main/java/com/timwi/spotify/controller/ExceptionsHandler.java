package com.timwi.spotify.controller;

import com.timwi.spotify.commons.exceptions.MissingRequestParamException;
import org.springframework.core.env.MissingRequiredPropertiesException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

@RestControllerAdvice
public class ExceptionsHandler extends ExceptionHandlerExceptionResolver {


    @ExceptionHandler(MissingRequiredPropertiesException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Object> objectNotFound(MissingRequiredPropertiesException e) {
        String body = "Missing required properties";
        if (e instanceof MissingRequestParamException) {
            body = e.getMessage();
        }
        return new ResponseEntity<>(body, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
