package com.timwi.spotify.controller;

import com.timwi.spotify.business.AlbumService;
import com.timwi.spotify.model.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/albums")
public class AlbumController {

    @Autowired
    private AlbumService albumService;

    @GetMapping()
    public List<Album> getAlbums(@RequestParam Optional<String> title, @RequestParam Optional<String> artist) {
        return albumService.getAlbums(title, artist);
    }

}
